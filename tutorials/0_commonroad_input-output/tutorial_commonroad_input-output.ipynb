{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Tutorial: CommonRoad Input-Output\n",
    "## Reading, Modifying, and Writing CommonRoad Scenarios\n",
    "\n",
    "This tutorial shows how CommonRoad XML-files can be read, modified, visualized, and stored. To start with, a CommonRoad XML-file consists of a **Scenario** and a **PlanningProblemSet**:\n",
    "* A **Scenario** represents the environment, which includes a **LaneletNetwork** and a set of **DynamicObstacle** and **StaticObstacle**.\n",
    "    * A **LaneletNetwork** consists of a set of lane segments (**Lanelets**) that can be connected arbitrarily.\n",
    "* A **PlanningProblemSet** contains one **PlanningProblem** for every ego vehicle in the **Scenario**, which in turn has an **initial position** and a to-be-reached **GoalRegion**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 0. Preparation\n",
    "* Before you proceed further, make sure you have skimmed through the [CommonRoad API](https://cps.pages.gitlab.lrz.de/commonroad/commonroad-io/api/index.html) to gain an overall view of the funtionalities provided by CommonRoad modules. You may need to refer to it for implementational details throughout this tutorial.\n",
    "\n",
    "* Additional documentations on **CommonRoad XML format, Cost Functions, Vehicle Models, etc.** can be found on the [CommonRoad](https://commonroad.in.tum.de/) website under their specific pages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Read XML file\n",
    "\n",
    "As documented in [CommonRoadFileReader](https://cps.pages.gitlab.lrz.de/commonroad/commonroad-io/api/common.html#commonroadfilereader-class), the **CommonRoadFileReader** reads in a CommonRoad XML file; its **open()** method returns a **Scenario** and a **PlanningProblemSet** object:"
   ]
  },
  {
   "cell_type": "code",
   "metadata": {},
   "source": [
    "import os\n",
    "import matplotlib.pyplot as plt\n",
    "plt.rcParams.update({'figure.max_open_warning': 0})\n",
    "from IPython.display import clear_output\n",
    "\n",
    "# import classes and functions for reading xml file and visualizing commonroad objects\n",
    "from commonroad.common.file_reader import CommonRoadFileReader\n",
    "from commonroad.visualization.mp_renderer import MPRenderer\n",
    "\n",
    "# generate path of the file to be read\n",
    "path_file = \"../../scenarios/tutorial/ZAM_Tutorial-1_2_T-1.xml\"\n",
    "\n",
    "# read in the scenario and planning problem set\n",
    "scenario, planning_problem_set = CommonRoadFileReader(path_file).open()\n",
    "\n",
    "# plot the scenario for 40 time steps, here each time step corresponds to 0.1 second\n",
    "for i in range(0, 40):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    renderer = MPRenderer()\n",
    "    \n",
    "    # plot the scenario for each time step\n",
    "    renderer.draw_params.time_begin = i\n",
    "    scenario.draw(renderer)\n",
    "    \n",
    "    # plot the planning problem set\n",
    "    planning_problem_set.draw(renderer)\n",
    "    \n",
    "    renderer.render()\n",
    "    plt.show()"
   ],
   "outputs": [],
   "execution_count": null
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "## 2. Modify XML file\n",
    "\n",
    "It is possible to modify existing CommonRoad scenarios to costumize them to one's needs. Here as an example, we would like to add a static obstacle to the scenario with the following specification:\n",
    "   * obstacle type: parked vehicle\n",
    "   * obstacle shape: rectangle with a width of 2.0 m and a length of 4.5 m\n",
    "   * initial state:\n",
    "        * position: (30, 3.5) m\n",
    "        * orientation: 0.02 rad\n",
    "   * obstacle id: since every object in the scenario must have a unique ID, we use the member function **generate_object_id()** of **Scenario** class to generate a unique ID for the object.\n",
    "\n",
    "As documented in [StaticObstacle](https://cps.pages.gitlab.lrz.de/commonroad/commonroad-io/api/scenario.html#staticobstacle-class), we need to provide `obstacle_id, obstacle_type, obstacle_shape, initial_state` to construct a static obstacle."
   ]
  },
  {
   "cell_type": "code",
   "metadata": {},
   "source": [
    "import numpy as np\n",
    "\n",
    "# import necesary classes from different modules\n",
    "from commonroad.geometry.shape import Rectangle\n",
    "from commonroad.scenario.obstacle import StaticObstacle, ObstacleType\n",
    "\n",
    "from commonroad.scenario.state import InitialState\n",
    "\n",
    "# read in the scenario and planning problem set\n",
    "scenario, planning_problem_set = CommonRoadFileReader(path_file).open()\n",
    "\n",
    "# generate the static obstacle according to the specification, \n",
    "# refer to API for details of input parameters\n",
    "static_obstacle_id = scenario.generate_object_id()\n",
    "static_obstacle_type = ObstacleType.PARKED_VEHICLE\n",
    "static_obstacle_shape = Rectangle(width = 2.0, length = 4.5)\n",
    "static_obstacle_initial_state = InitialState(position = np.array([30.0, 3.5]), \n",
    "                                      orientation = 0.02, time_step = 0)\n",
    "\n",
    "# feed in the required components to construct a static obstacle\n",
    "static_obstacle = StaticObstacle(static_obstacle_id, static_obstacle_type, \n",
    "                                 static_obstacle_shape, static_obstacle_initial_state)\n",
    "\n",
    "# add the static obstacle to the scenario\n",
    "scenario.add_objects(static_obstacle)\n",
    "    \n",
    "# plot the scenario for each time step\n",
    "for i in range(0, 40):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    renderer = MPRenderer()\n",
    "    \n",
    "    # plot the scenario for each time step\n",
    "    renderer.draw_params.time_begin = i\n",
    "    scenario.draw(renderer)\n",
    "    \n",
    "    # plot the planning problem set\n",
    "    planning_problem_set.draw(renderer)\n",
    "    \n",
    "    renderer.render()\n",
    "    plt.show()"
   ],
   "outputs": [],
   "execution_count": null
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen from the output, we have successfully added a new static obstacle to the scenario. We further add a dynamic obstacle with the following specifications:\n",
    "\n",
    "   * obstacle type: car\n",
    "   * obstacle shape: rectangle with a width of 1.8 m and a length of 4.3 m\n",
    "   * initial state:\n",
    "        * position: (50, 0.0) m\n",
    "        * orientation: 0.00 rad\n",
    "        * velocity: 22 m/s in the positive direction of x-axis\n",
    "   * we assume that the dynamic obstacle drives with constant velocity.\n",
    "\n",
    "As documented in [DynamicObstacle](https://cps.pages.gitlab.lrz.de/commonroad/commonroad-io/api/scenario.html#dynamicobstacle-class), we need to pass in a **Prediction** object, which in this case is a **TrajectoryPrediction** object. Generation of the trajectory prediction goes as follows:\n",
    "   1. compute all subsequent states of the dynamic obstacle\n",
    "   2. create a **Trajectory** object from these states\n",
    "   3. create a **TrajectoryPrediction** object from this **Trajectory** and the shape of the dynamic obstacle"
   ]
  },
  {
   "cell_type": "code",
   "metadata": {},
   "source": [
    "# import necesary classes from different modules\n",
    "from commonroad.scenario.obstacle import DynamicObstacle\n",
    "from commonroad.scenario.trajectory import Trajectory\n",
    "from commonroad.prediction.prediction import TrajectoryPrediction\n",
    "\n",
    "from commonroad.scenario.state import KSState\n",
    "\n",
    "dynamic_obstacle_id = scenario.generate_object_id()\n",
    "dynamic_obstacle_type = ObstacleType.CAR\n",
    "constant_velocity = 22\n",
    "# initial state has a time step of 0\n",
    "dynamic_obstacle_initial_state = InitialState(position = np.array([50.0, 0.0]), \n",
    "                                       velocity = constant_velocity,\n",
    "                                       orientation = 0.0, \n",
    "                                       time_step = 0)\n",
    "\n",
    "# generate the states of the obstacle for time steps 1 to 40\n",
    "state_list = []\n",
    "for i in range(1, 40):\n",
    "    # compute new position, here scenario.dt = 0.1 s\n",
    "    new_position = np.array([dynamic_obstacle_initial_state.position[0] + \n",
    "                             scenario.dt * i * constant_velocity, 0])\n",
    "    # create new state\n",
    "    new_state = KSState(position = new_position, velocity = constant_velocity,\n",
    "                      orientation = 0.02, time_step = i)\n",
    "    # add new state to state_list\n",
    "    state_list.append(new_state)\n",
    "\n",
    "# create the trajectory of the obstacle starting at time step 1\n",
    "dynamic_obstacle_trajectory = Trajectory(1, state_list)\n",
    "\n",
    "# create the prediction using the trajectory and the shape of the obstacle\n",
    "dynamic_obstacle_shape = Rectangle(width = 1.8, length = 4.3)\n",
    "dynamic_obstacle_prediction = TrajectoryPrediction(dynamic_obstacle_trajectory, \n",
    "                                                   dynamic_obstacle_shape)\n",
    "\n",
    "# generate the dynamic obstacle according to the specification\n",
    "dynamic_obstacle_id = scenario.generate_object_id()\n",
    "dynamic_obstacle_type = ObstacleType.CAR\n",
    "dynamic_obstacle = DynamicObstacle(dynamic_obstacle_id, \n",
    "                                   dynamic_obstacle_type, \n",
    "                                   dynamic_obstacle_shape, \n",
    "                                   dynamic_obstacle_initial_state, \n",
    "                                   dynamic_obstacle_prediction)\n",
    "\n",
    "# add dynamic obstacle to the scenario\n",
    "scenario.add_objects(dynamic_obstacle)\n",
    "\n",
    "# plot the scenario for each time step\n",
    "for i in range(0, 40):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    renderer = MPRenderer()\n",
    "    \n",
    "    # plot the scenario for each time step\n",
    "    renderer.draw_params.time_begin = i\n",
    "    scenario.draw(renderer)\n",
    "    \n",
    "    # plot the planning problem set\n",
    "    planning_problem_set.draw(renderer)\n",
    "    \n",
    "    renderer.render()\n",
    "    plt.show()"
   ],
   "outputs": [],
   "execution_count": null
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Write XML file\n",
    "After we have modified the scenario, we would like to save the changes and write the **Scenario** and **PlanningProblemSet** to a CommonRoad XML file. [CommonRoadFileWriter](https://cps.pages.gitlab.lrz.de/commonroad/commonroad-io/api/common.html#commonroadfilewriter-class) helps us to achieve this. The scenarios can be labeled with different [tags](https://cps.pages.gitlab.lrz.de/commonroad/commonroad-io/api/scenario.html#tag-class) describing them.\n",
    "Note that we did not modify the **PlanningProblemSet** in this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "metadata": {},
   "source": [
    "# import necesary classes from different modules\n",
    "from commonroad.common.file_writer import CommonRoadFileWriter\n",
    "from commonroad.common.file_writer import OverwriteExistingFile\n",
    "from commonroad.scenario.scenario import Location\n",
    "from commonroad.scenario.scenario import Tag\n",
    "\n",
    "author = 'Max Mustermann'\n",
    "affiliation = 'Technical University of Munich, Germany'\n",
    "# where the scenario came from\n",
    "source = ''\n",
    "# tags of the sceanrio\n",
    "tags = {Tag.CRITICAL, Tag.INTERSTATE}\n",
    "\n",
    "# create a CommonRoad file writer\n",
    "fw = CommonRoadFileWriter(scenario, planning_problem_set, author, affiliation, source, tags)\n",
    "\n",
    "# write the scenario to the current directory (where this Jupyter Notebook is located)\n",
    "name_file = \"../../scenarios/tutorial/ZAM_Tutorial-1_2_T-2.xml\"\n",
    "fw.write_to_file(name_file, OverwriteExistingFile.ALWAYS)"
   ],
   "outputs": [],
   "execution_count": null
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can open our saved file again to check if everything is correct:"
   ]
  },
  {
   "cell_type": "code",
   "metadata": {},
   "source": [
    "path_file = \"../../scenarios/tutorial/ZAM_Tutorial-1_2_T-2.xml\"\n",
    "\n",
    "scenario, planning_problem_set = CommonRoadFileReader(path_file).open()\n",
    "\n",
    "# plot the scenario for each time step\n",
    "for i in range(0, 40):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    renderer = MPRenderer()\n",
    "    \n",
    "    # plot the scenario for each time step\n",
    "    renderer.draw_params.time_begin = i\n",
    "    scenario.draw(renderer)\n",
    "    \n",
    "    # plot the planning problem set\n",
    "    planning_problem_set.draw(renderer)\n",
    "    \n",
    "    renderer.render()\n",
    "    plt.show()"
   ],
   "outputs": [],
   "execution_count": null
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Congratulations!\n",
    "\n",
    "You have finished the tutorial on CommonRoad Input-Output!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.15"
  },
  "vscode": {
   "interpreter": {
    "hash": "2f6eab2d63de360a1b566290c617c31ba0315e2d992e7daa4351f45c5ea4c1ba"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
